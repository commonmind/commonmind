# Adapted "in spirit" from scripts by Noam Beckmann and Joe Scarpa.
#
# Uses Fisher's Exact test to find the "best match" in geneAnnots2, for each annot in geneAnnots1
#
findBestGeneSetMatches <- function(geneAnnots1, geneAnnots2, HANDLE_GENES="union", FISHER_ALTERNATIVE="greater", P_ADJUST_ALL_ANNOTS1=TRUE, P_ADJUST_METHOD="bonferroni", MAX_P_ADJUST=0.05) {

    if (HANDLE_GENES == "intersect") {
        GENES = intersect(names(geneAnnots1$geneToAnnots), names(geneAnnots2$geneToAnnots))
    
        if (length(geneAnnots1$geneToAnnots) > length(GENES)) {
            writeLines(paste("Excluding ", length(geneAnnots1$geneToAnnots) - length(GENES), " gene(s) from geneAnnots1 (those not present in geneAnnots2)", sep=""))
            geneAnnots1 = subsetGeneAnnots(geneAnnots1, GENES)
        }
        if (length(geneAnnots2$geneToAnnots) > length(GENES)) {
            writeLines(paste("Excluding ", length(geneAnnots2$geneToAnnots) - length(GENES), " gene(s) from geneAnnots2 (those not present in geneAnnots1)", sep=""))
            geneAnnots2 = subsetGeneAnnots(geneAnnots2, GENES)
        }
    } else if (HANDLE_GENES == "union") {
        GENES = union(names(geneAnnots1$geneToAnnots), names(geneAnnots2$geneToAnnots))
    } else {
        stop(paste("Invalid HANDLE_GENES argument: ", HANDLE_GENES, sep=""))
    }
    NUM_GENES = length(GENES)

    annotSizes1 = sort(sapply(geneAnnots1$annotToGenes, length, simplify=TRUE), decreasing=TRUE)
    annotSizes2 = sort(sapply(geneAnnots2$annotToGenes, length, simplify=TRUE), decreasing=TRUE)
    
    uniqueAnnots1 = names(annotSizes1)
    uniqueAnnots2 = names(annotSizes2)

    BASIC_1_BY_2_MAT = function(defaultData) {matrix(data=defaultData, nrow=length(uniqueAnnots1), ncol=length(uniqueAnnots2), dimnames=list(uniqueAnnots1, uniqueAnnots2))}

    overlap = BASIC_1_BY_2_MAT(NA)
    unionGenes = BASIC_1_BY_2_MAT(NA)
    annotGenesUniqueTo1 = BASIC_1_BY_2_MAT(NA)
    annotGenesUniqueTo2 = BASIC_1_BY_2_MAT(NA)
    pvalMat = BASIC_1_BY_2_MAT(NA)
    adjustPvalMat = BASIC_1_BY_2_MAT(NA)    
    oddsRatioMat = BASIC_1_BY_2_MAT(NA)
    oddsRatioLBmat = BASIC_1_BY_2_MAT(NA)
    oddsRatioUBmat = BASIC_1_BY_2_MAT(NA)

    for (i in 1:length(uniqueAnnots1)) {
	iAnnot1 = uniqueAnnots1[i]
        i1Genes = geneAnnots1$annotToGenes[[iAnnot1]]
        
	for (j in 1:length(uniqueAnnots2)) {
            jAnnot2 = uniqueAnnots2[j]
            j2Genes = geneAnnots2$annotToGenes[[jAnnot2]]
            
            overlap[i, j] = length(intersect(i1Genes, j2Genes))
            annotGenesUniqueTo1[i, j] = length(setdiff(i1Genes, j2Genes))
            annotGenesUniqueTo2[i, j] = length(setdiff(j2Genes, i1Genes))
            unionGenes[i, j] = length(union(i1Genes, j2Genes))

            genesNotInEitherAnnot = NUM_GENES - unionGenes[i, j]
            fishmat = matrix(data=c(overlap[i, j], annotGenesUniqueTo1[i, j], annotGenesUniqueTo2[i, j], genesNotInEitherAnnot), nrow=2, ncol=2, byrow=TRUE)
            
            testResult = fisher.test(fishmat, alternative=FISHER_ALTERNATIVE)
            pvalMat[i, j] = testResult$p.value
            oddsRatioMat[i, j] = testResult$estimate
            oddsRatioLBmat[i, j] = testResult$conf.int[1]
            oddsRatioUBmat[i, j] = testResult$conf.int[2]
        }

        if (!P_ADJUST_ALL_ANNOTS1) {
            adjustPvalMat[i, ] = p.adjust(pvalMat[i, ], method=P_ADJUST_METHOD)
        }
    }

    if (P_ADJUST_ALL_ANNOTS1) {
        allRowCols = ind2sub(dim(pvalMat), 1:(nrow(pvalMat) * ncol(pvalMat)))
        # Valid, as described here: http://stackoverflow.com/questions/6920441/index-values-from-a-matrix-using-row-col-indicies
        adjustPvalMat[allRowCols] = p.adjust(as.numeric(unlist(pvalMat)), method=P_ADJUST_METHOD)
    }

    annotateRowCols = ind2sub(dim(adjustPvalMat), which(adjustPvalMat <= MAX_P_ADJUST))
    
    annotCols = c("annot1", "annot2", "size1", "size2", "intersect", "size1_frac", "jaccard", "p", "adjP", "OR", "OR_LB", "OR_UB")
    annotOutput = as.data.frame(matrix(NA, nrow=nrow(annotateRowCols), ncol=length(annotCols)), stringsAsFactors=FALSE, check.names=FALSE)
    colnames(annotOutput) = annotCols

    if (nrow(annotateRowCols) > 0) {
        for (ind in 1:nrow(annotateRowCols)) {
            row = annotateRowCols[ind, 1]
            col = annotateRowCols[ind, 2]

            size1 = as.numeric(annotSizes1[row])
            size2 = as.numeric(annotSizes2[col])
            common = overlap[row, col]
            size1_frac = common / size1
            jaccard = common / unionGenes[row, col]
        
            addAnnot = data.frame(uniqueAnnots1[row], uniqueAnnots2[col], size1, size2, common, size1_frac, jaccard, pvalMat[row, col], adjustPvalMat[row, col], oddsRatioMat[row, col], oddsRatioLBmat[row, col], oddsRatioUBmat[row, col], stringsAsFactors=FALSE, check.names=FALSE)
            annotOutput[ind, ] = addAnnot
        }
    }

    sortVals = annotOutput[, "size1"]
    names(sortVals) = annotOutput[, "annot1"] # Add names to induce a stable sort, as per help(sort)...
    useInds = sort(sortVals, index.return=TRUE, decreasing=TRUE)$ix
    annotOutput = annotOutput[useInds, ]
    
    return(annotOutput)
}


findBestModuleMatchesFromFiles <- function(modules1File, modules2File) {
    findBestGeneSetMatches(modulesDataToGeneAnnotations(readModulesFile(modules1File)), modulesDataToGeneAnnotations(readModulesFile(modules2File)))
}
