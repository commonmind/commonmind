# Combine enrichment scores using Fishers test with Brown's correction or IDR based approach
library(idr)
library(plyr)

COMBINE_SCORES <- function(SCORES,
                           SUB_METHOD = 'FB' # options are FB or IDR
                           ){
  # Calculate Fishers chi-square stat and p-value with Brown's correction
  if (SUB_METHOD == 'FB'){
    N <- dim(SCORES)[2]
    SCORES[apply(SCORES,2,is.infinite)] <- 1000 # Approximate all infinite scores to 1000 for calculation purpose
    
    correlation <- cor(SCORES);
    S <- 0
    for (i in 1:(N-1)){
      for (j in (i+1):N){
        S <- S + correlation[i,j]*(3.25+0.75*correlation[i,j])
      }
    }
    FB.ChiSquare <- rowSums(SCORES)*(4*N)/(4*N+2*S)
    FB.Pval <- sapply(FB.ChiSquare, function(x,N){pchisq(x, df = 2*N, lower.tail = FALSE)},N)
    FB.Pval <- p.adjust(FB.Pval,'BH')
    
    return(list(PVAL.CORRELATION = correlation, FB.Pval = FB.Pval))
    
  } else if (SUB_METHOD == 'IDR'){
    
    # Initial guess for theta
    N = dim(SCORES)[2]/2
    THRESHOLD <- median(as.matrix(SCORES))
    Mu <- median(apply(SCORES[rowSums(SCORES>=THRESHOLD)>=N,],1,mean))
    sigma <- median(apply(SCORES[rowSums(SCORES>=THRESHOLD)>=N,],1,sd))
    rho <- median(cor(t(SCORES[rowSums(SCORES>=THRESHOLD)>=N,])))
    p <- sum(rowSums(SCORES>=THRESHOLD)>=N)/dim(SCORES)[1]
    
    # Calculate IDR
    estIDR <- est.IDR(SCORES,Mu,sigma,rho,p,max.ite=100)
    
    # Combine results
    return(list(APVAL.CORRELTION = cor(SCORES), IDR = estIDR))
    
  } else{
    error('SUB_METHOD has to be FB or IDR')
  } 
}